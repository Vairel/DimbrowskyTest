function changeRating(event, ratingComponent, removeClassName, addClassName) {
    if(event.target.tagName === "LI"){
        let ratingStars = event.currentTarget.querySelectorAll("li");
        if(removeClassName !== ""){
            for(let i = 0; i < ratingStars.length; ++i){
                ratingStars[i].classList.remove(removeClassName);
            }
        }
        if(addClassName !== ""){
            for(let i = 0; i < ratingStars.length; ++i){
                ratingStars[i].classList.add(addClassName);
                if(ratingStars[i] === event.target){
                    break;
                }
            }
        }
    }
}
let ratingComponent = document.getElementById("ratingComponent");
ratingComponent.addEventListener("click", function (event) {
    changeRating(event, ratingComponent, "current", "current");
});
ratingComponent.addEventListener("mouseover", function (event) {
    changeRating(event, ratingComponent, "hovered", "hovered");
});
ratingComponent.addEventListener("mouseout", function (event) {
    changeRating(event, ratingComponent, "hovered", "");
});

function incorrect(someObject){
    someObject.classList.remove("correct");
}
window.addEventListener("load", function () {
    let list = document.getElementById("imageList");
    let img = list.getElementsByTagName("img");
    for(let i = 0; i < img.length; ++i){
        if(!img[i].complete) {
            incorrect(list);
            break;
        }
    }
});